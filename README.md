# important links

* https://github.com/iptv-org/iptv
* https://github.com/iptv-org/awesome-iptv
* https://www.whatsuptv.app/ used for testing playlists
* main source files
    * ae: https://iptv-org.github.io/iptv/countries/ae.m3u
    * bh: https://iptv-org.github.io/iptv/countries/bh.m3u
    * in: https://iptv-org.github.io/iptv/countries/in.m3u
    * pk: https://iptv-org.github.io/iptv/countries/pk.m3u
    * sa: https://iptv-org.github.io/iptv/countries/sa.m3u
    * hindi: https://iptv-org.github.io/iptv/languages/hin.m3u
    * urdu: https://iptv-org.github.io/iptv/languages/urd.m3u
* expression to add new lines before each channel (regex)
    * search for: #[A-Z].*
    * replace with: \n$0